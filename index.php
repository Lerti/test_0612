<?php

include_once("controllers/SiteController.php");

$action = 'action_';
$action .= (isset($_GET['act'])) ? $_GET['act'] : 'index';

$controller = new SiteController();
$controller->Request($action);
