<?php

include_once ('models/Tarifs.php');

class SiteController
{
    protected $title;
    protected $content;
    protected $library;

    protected function before() {
        $this->title = 'Тестовое';
        $this->content = '';
        $this->library = '';
    }

    /**
     * Generate pages
     * @param $action string
     */
    public function Request(string $action) {
        $this->before();
        $this->$action();
        $this->render();
    }

    /**
     * Generate html template in line
     * @param $fileName string
     * @param $vars array
     * @return int|false the length of the output buffer contents or false if no
     * buffering is active.
     */
    protected function Template(string $fileName, array $vars = []) {
        foreach ($vars as $k => $v) {
            $$k = $v;
        }
        ob_start();
        include "$fileName";
        return ob_get_clean();
    }

    /**
     * Display general part of pages
     * generate template views/main/main.php
     */
    public function render() {
        $vars = array('title' => $this->title, 'content' => $this->content, 'library' => $this->library);

        $page = $this->Template('views/main/main.php', $vars);
        echo $page;
    }

    /**
     * Display main page
     * generate template views/site/master.php
     */
    public function action_index() {
        $this->title .= ' | Главная';
        $tarifs = Tarifs::getTarifs();
        $this->library = '<script src = "js/jquery-3.0.0.min.js"></script><script src = "js/main.js"></script>';
        $this->content = $this->Template('views/site/main.php',
            ['tarifs' => $tarifs, 'library' => $this->library]);
    }
}
