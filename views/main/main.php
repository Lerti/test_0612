<?php
/**
 * view of main page
 * @var $title string
 * @var $content array
 * @var $library string
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title><?= $title ?></title>
    <meta content="text/html; charset =UTF-81" http-equiv="content-type">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/site.css"/>
</head>
<body>
<div id="content">
    <?= $content ?>
</div>
<?= $library ?>
</body>
</html>
