<?php
/**
 * @var $tarifs array
 */

if (key_exists('error', $tarifs)) {
    echo $tarifs['error'];
} else {
    foreach ($tarifs as $tarif): ?>
        <div class="tarif <?= $tarif['class'] ?>" data-id="<?= $tarif['id'] ?>">
            <div class="name">
                <p>Тариф "<?= $tarif['title'] ?>"</p>
            </div>
            <div class="tarif-content-div">
                <div class="speed-price-div">
                    <div>
                        <div class="speed">
                            <p><?= $tarif['speed'] ?> Мбит/с</p>
                        </div>
                        <div class="price">
                            <p><?= $tarif['price'] ?> &#8381;/мес</p>
                        </div>
                    </div>
                    <div class="one-arrow first-arrow">
                        <img src="/images/arrow-gray.svg" alt="стрелка"">
                    </div>
                </div>
                <?php if (key_exists('free_options', $tarif)) : ?>
                    <div class="free-options">
                        <?php foreach ($tarif['free_options'] as $option) { ?>
                            <p><?= $option ?></p>
                        <?php } ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="link">
                <a href="<?= $tarif['link'] ?>">узнать подробнее на сайте www.sknt.ru</a>
            </div>
        </div>
        <div class="tarif-two tarif-second" data-id="<?= $tarif['id'] ?>">
            <div class="tarif-name-div">
                <div class="two-arrow second-arrow">
                    <img src="/images/arrow-green.svg" alt="стрелка"">
                </div>
                <div class="name">
                    <p>Тариф "<?= $tarif['title'] ?>"</p>
                </div>
            </div>
            <?php foreach ($tarif['tarifs'] as $array) { ?>
                <div class="tarif-content-div">
                    <div>
                        <div class="name">
                            <p><?= $array['name'] ?></p>
                        </div>
                    </div>
                    <div class="speed-price-div">
                        <div>
                            <div class="price">
                                <p><?= $array['price'] ?> &#8381;/мес</p>
                            </div>
                            <div class="big-price">
                                <p>разовый платёж &#8211; <?= $array['allPrice'] ?> &#8381;</p>
                                <?php if ($array['sale'] !== 0) : ?>
                                    <div>
                                        <p>скидка &#8211; <?= $array['sale'] ?> &#8381;</p>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="one-arrow third-arrow">
                            <img src="/images/arrow-gray.svg" alt="стрелка" data-small-id="<?= $array['id'] ?>">
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <?php foreach ($tarif['tarifs'] as $array) { ?>
            <div class="tarif-two tarif-third" data-id="<?= $tarif['id'] ?>" data-small-id="<?= $array['id'] ?>">
                <div class="tarif-name-div">
                    <div class="two-arrow fourth-arrow">
                        <img src="/images/arrow-green.svg" alt="стрелка"">
                    </div>
                    <div class="name">
                        <p>Выбор тарифа</p>
                    </div>
                </div>
                <div class="tarif-content-div">
                    <div>
                        <div class="name">
                            <p>Тариф "<?= $tarif['title'] ?>"</p>
                        </div>
                    </div>
                    <div class="speed-price-div">
                        <div class="price">
                            <p>Период оплаты &#8211; <?= $array['name'] ?></p>
                            <p><?= $tarif['maxPrice'] ?> &#8381;/мес</p>
                        </div>
                        <div class="big-price">
                            <p>разовый платёж &#8211; <?= $array['allPrice'] ?> &#8381;</p>
                            <p>со счёта спишется &#8211; <?= $array['allPrice'] ?> &#8381;</p>
                        </div>
                        <div class="date-div">
                            <p>вступит в силу &#8211; сегодня</p>
                            <p>активно до &#8211; <?= $array['new_payday'] ?></p>
                        </div>
                        <div class="button">Выбрать</div>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php endforeach;
}
