<?php

class Tarifs
{
    const URL = 'https://www.sknt.ru/job/frontend/data.json';
    const URL_IS_OK = 'ok';

    public static function getTarifs(): array
    {
        $site = file_get_contents(self::URL);
        $tarifs = json_decode($site, true);
        $tarifsArray = [];
        if ($tarifs['result'] === self::URL_IS_OK) {
            foreach ($tarifs['tarifs'] as $array) {
                $minPrice = 0;
                $maxPrice = 0;
                $newTarifs = [];
                foreach ($array['tarifs'] as $one) {
                    $price = $one['price'] / $one['pay_period'];
                    $minPrice = ($minPrice === 0 || $price < $minPrice) ? $price : $minPrice;
                    $maxPrice = $price > $maxPrice ? $price : $maxPrice;
                }
                foreach ($array['tarifs'] as $one) {
                    $newTarif = [];
                    $price = $one['price'] / $one['pay_period'];
                    $newTarif['id'] = $one['ID'];
                    $newTarif['name'] = $one['pay_period'] . ' месяц' .
                        ($one['pay_period'] == 1 ? '' : ($one['pay_period'] == 3 ? 'а' : 'ев'));
                    $newTarif['price'] = $price;
                    $newTarif['allPrice'] = $one['price'];
                    $newTarif['sale'] = ($maxPrice - $price) * $one['pay_period'];
                    $newTarif['pay_period'] = $one['pay_period'];
                    $newTarif['new_payday'] = date("d.m.Y", $one['new_payday']);
                    $newTarifs[] = $newTarif;
                }

                $array['price'] = $minPrice . ' &#8211; ' . $maxPrice;
                $array['maxPrice'] = $maxPrice;
                $array['class'] = (strpos($array['title'], 'Земля') !== false) ? 'land' : (
                (strpos($array['title'], 'Огонь') !== false) ? 'fire' : 'water'
                );
                usort($newTarifs, function($a, $b) {
                    return ($a['id'] - $b['id']);
                });
                $array['tarifs'] = $newTarifs;
                $array['id'] = $i++;
                $tarifsArray[] = $array;
            }
        } else {
            return ['error' => 'Произошла ошибка при попытке получить доступ к данным на сервере. Попробуйте еще раз.'];
        }
        return $tarifsArray;
    }
}
