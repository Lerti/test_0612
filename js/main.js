$(document).ready(() => {
    $('.first-arrow').click((e) => {
        let id = $(e.currentTarget).closest(".tarif").data('id');
        $('.tarif').hide();
        $(".tarif-second[data-id='" + id + "']").show();
    });

    $('.second-arrow').click((e) => {
        let id = $(e.currentTarget).closest(".tarif-two").data('id');
        $(".tarif-second[data-id='" + id + "']").hide();
        $('.tarif').show();
    });

    $('.third-arrow').click((e) => {
        let id = $(e.currentTarget).closest(".tarif-two").data('id');
        console.log(id);

        let smallId = $(e.currentTarget).children().data('small-id');
        console.log(smallId);
        $(".tarif-second[data-id='" + id + "']").hide();
        console.log($(".tarif-third[data-id='" + id + "']" + "[data-small-id='" + smallId + "']"));
        console.log($(".tarif-third[data-id='" + smallId + "']"));
        console.log($(".tarif-third[data-small-id='" + smallId + "']"));
        $(".tarif-third[data-id='" + id + "']" + "[data-small-id='" + smallId + "']").show();
    });

    $('.fourth-arrow').click((e) => {
        let id = $(e.currentTarget).closest(".tarif-third").data('id');
        let smallId = $(e.currentTarget).closest(".tarif-third").data('small-id');
        $(".tarif-third[data-id='" + id + "']" + "[data-small-id='" + smallId + "']").hide();
        $(".tarif-second[data-id='" + id + "']").show();
    });
});
